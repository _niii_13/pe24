#include<stdio.h>
int loop(int n)
{
	int i = 0, sum = 0;
	while (i < n) {
		sum = sum + i;
		i++;
	}
	return sum;
}

int main()
{
	int sum, n;
	printf("Enter the value of n \n");
	scanf("%d", &n);
	sum = loop(n);
	printf("The sum of first 'n' numbers is = %d \n", sum);
	return 0;
}